# Overview

This is a project for running firebase-emulator locally.

## Prerequisites

- Install [firebase-cli](https://firebase.google.com/docs/cli)
- Install [Node.js](https://nodejs.org/) version 8.0 or higher.
- Install [Java](https://openjdk.java.net/install/) version 1.8 or higher.

## Steps

1. Open terminal then login to your firebase account - `firebase login`
2. Still in the termina, go to the folder where you checked-out firebase-emulator - eg, `cd ~/firebase-emulator`
3. Lastly, execute `firebase emulators:start --import=./data --export-on-exit`